package com.example.springsecurity.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.springsecurity.models.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {

}
